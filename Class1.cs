﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp4
{
    public class Person
    {
        private int idPesel;
        private string firstName;
        private string lastName;
        private DateTime dateofBirth;
        public enum Gender
        {
            Male = 1,
            Female = 2
        }
        public Gender gender;

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                this.firstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                this.lastName = value;
            }
        }

        public Gender GendeR
        {
            get
            {
                return gender;
            }
            set
            {
                this.gender = value;
            }
        }

        public string GetFullName()
        {
            string full_name = this.firstName + this.lastName;
            return full_name; 
        }

        public void SetAge(int day, int month, int year)
        {
            this.dateofBirth = new DateTime(year, month, day);
        }

        public bool CheckPesel(int idPesel, DateTime dateOfBirth)
        {
            if (dateOfBirth != idPesel)
                return false;
            else
                return true;
        }

       string.isNullorEmpty(input)

        public Person(int idPesel, string firstName, string lastName, DateTime dateofBirth, Gender gender )
        {
            this.idPesel = idPesel;
            this.firstName = firstName;
            this.lastName = lastName;
            this.dateofBirth = dateofBirth;
            this.gender = gender;
        }

        public Person(int idPesel)
        {
            this.idPesel = idPesel; 
        }

        public Person(int idPesel, string firstName, string lastName) : this(idPesel)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public Person(int idPesel, string firstName, string LastName, DateTime dateofBirth) : this(idPesel, firstName, lastName)
        {
            this.dateofBirth = dateofBirth;
        }

        public void SetGender(Gender gender)
        {
            this.gender = gender;
        }


       
    }

    
}
